<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsultationRoom extends Model
{
	protected $table = 'consultation_room';
	public $timestamps = false;
}
