<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\ConsultationRoom;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
	/*
	 * Show doctor's dashboard
	 * @return Response
	 */
	public function dashBoard()
	{
		$patientsInConsultationRoom = ConsultationRoom::all();
		$patientsInConsultationRoom->sortBy('created_date');
		return view('doctor-dashboard',[ 'patientsInConsultationRoom' => $patientsInConsultationRoom ]);
	}	
	
	/*
	 * get list patients in consultation room
	 * @return: json
	 */
	public function getPatientsInConsultationRoom()
	{
		$patientsInConsultationRoom = ConsultationRoom::all();
		$patientsInConsultationRoom->sortBy('created_date');
		return response()->json([ 'patientsInConsultationRoom' => $patientsInConsultationRoom ]);
	}
	
	/*
	 * update patient waiting time when patient is in consultation room
	 */
	public function updateConsultationPatient(Request $request)
	{
		$patientsInConsultationRoom = ConsultationRoom::find($request->id);
		$patientsInConsultationRoom->minuteWaiting = $request->minuteWaiting;
		$patientsInConsultationRoom->save();
		return response()->json([ 'responseCode' => 1,'message' => 'Success' ]);
	}
}