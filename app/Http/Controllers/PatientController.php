<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ConsultationRoom;

class PatientController extends Controller
{
	/*
	 * Show waiting room
	 * @return Response
	 */
	public function waitingRoom()
	{
		// force redirect to consultation room if patient is there and want to access waiting room again
		if(session('patient') != null)
		{
			return redirect('/consultation-room');
		}
		
		return view('waiting-room');
	}
	
	/*
	 * handle request enter consultation room
	 * request type : post
	 */
	public function enterConsultationRoom(Request $request)
	{		
		// validate input
		if($request->name == '')
		{
			return response()->json([ 'responseCode' => -1,'message' => 'Parameter name is required.' ],403);
		}
		
		if($request->vseeId== '')
		{
			return response()->json([ 'responseCode' => -1,'message' => 'Parameter vseeId is required.' ],403);
		}		
		
		$consultationRoom = new ConsultationRoom();
		$consultationRoom->patient_name = $request->name;
		$consultationRoom->reason = $request->reason;
		$consultationRoom->vseeId = $request->vseeId;
		$consultationRoom->minuteWaiting = 0;
		$consultationRoom->created_date = date('Y/m/d h:i:s');
		$result = $consultationRoom->save();
		if($result)
		{
			session([ 'patient' => $consultationRoom ]);
			return response()->json([ 'responseCode' => 1,'message' => 'Enter consultation room success','data' => $consultationRoom->jsonSerialize() ]);
		}
		else 
		{
			return response()->json([ 'responseCode' => 0,'message' => 'Enter consultation room fail' ]);
		}
	}
	
	/*
	 * handle request exit consultation room
	 */
	public function exitConsultationRoom(Request $request)
	{
		$consultationRoom = session('patient');
		$vseeId = $consultationRoom->vseeId;
		$consultationRoom->delete();
		session()->forget('patient');
		return response()->json([ 'responseCode' => 1,'message' => 'Exit consultation room success','data' => $vseeId ]);
	}
	
	/**
	 * Show the consultation room.
	 *
	 * @return Response
	 */
	public function consultationRoom()
	{
		$consultationRoom = session('patient');
		
		// prevent directly access to consultation room
		if($consultationRoom == null)
		{
			return redirect('/waiting-room');			
		}
		return view('consultation-room',[ 'patient' => session('patient') ]);
	}
}