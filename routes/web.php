<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * patient route
 */
Route::get('/', 'PatientController@waitingRoom');
Route::get('/waiting-room', 'PatientController@waitingRoom');
Route::post('/enter-consultation-room','PatientController@enterConsultationRoom');
Route::get('/exit-consultation-room','PatientController@exitConsultationRoom');
Route::get('/consultation-room', 'PatientController@consultationRoom');

/*
* doctor route
*/
Route::get('/doctor-dashboard','DoctorController@dashBoard');
Route::get('/patients-in-consultation-room','DoctorController@getPatientsInConsultationRoom');
Route::post('/update-consultation-patient','DoctorController@updateConsultationPatient');