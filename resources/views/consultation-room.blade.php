@extends('layouts.layout')
@section('content')
<div class="container-fluid">
 
	<div class="row">
	 
		<div class="col-md-12 navbar">
			<img src="https://i1.wp.com/vsee.com/wp-content/uploads/2015/05/VSee_Lab_Logo.png" alt="Logo" width="180" />			
			
				
				<div style="float: right;display: block;width: 200px;">
					<img alt="" src="/images/computer.png" height="17">&nbsp;Test Computer
				</div>
						
			<hr class="w-100"/>							
		</div>		
	</div>

	<div class="row">
		<div class="col-md-4">
		</div>
		
		<div class="col-md-4 margin-from-header">
			<div class="text-center">
				<h3 class="waiting-room-header">Welcome to Code Challenge Waiting Room</h3>
				<h6>If this is an emergency, please call 911.</h6>
			</div>
			
			<ul class="list-group">
			  	<li class="list-group-item list-group-item-success" style="background-color: #68a856"><img alt="" src="/images/time-icon.png"><span class="font-weight-bold text-white">&nbsp;Connecting with your provider</span></li>
			  	<li class="list-group-item">
			  		<div style="margin-top: 67px;font-weight: bolder;" class="text-center">
			  			<span id="message">Your provider will be with you shortly.</span>
			  		</div>
			  		<div class="text-center">
			  			<input type="hidden" id="vseeId" value="{{$patient->vseeId}}" />
			  			<button class="btn btn-warning exit-waiting-room-button" data-bind="click: exitWaitingRoomClick"><span class="font-weight-bold text-white">Exit Waiting Room</span></button>
			  		</div>
			  		
			  		<hr class="w-100">
			  		<div class="consultation-room-notice">
			  			If you close the video conference by mistake please, <a href="#">click here to relaunch video</a> again.
			  		</div>  		
			  	</li>			  
			</ul>
			
		</div>
		
		<div class="col-md-4">
		</div>
	</div>
</div>
<script type="text/javascript">
var ws;
if('WebSocket' in window)
{    
   ws = new WebSocket('ws://vseetest.com:8080');
   ws.onmessage = function(message){
     var responseData = JSON.parse(message.data);
     switch(responseData.message)
 		{
 		   case 'doctorCall':
 		      if($('#vseeId').val() == responseData.vseeId)
 		      {
			     $('#message').html('The visit is in progress');
 	 		  }
 		      else
 		      {
 		       $('#message').html('Doctor is currently busy and will attend to you soon');
 	 		  }
 			  break; 		   
 		}
   };
}

var viewModel = {
   exitWaitingRoomClick: function(){
      $.getJSON("/exit-consultation-room", function(returnedData) {
        if(returnedData.responseCode == 1)
        {
           if(ws != null)
           {
             ws.send(JSON.stringify({ 
      	        'message':'patientExitConsultationRoom',
      		    'vseeId': returnedData.data
      	     }));
           } 
           location.href = '/waiting-room';            

        }
   	  }); 
   }
}
ko.applyBindings(viewModel);
</script>
@endsection
