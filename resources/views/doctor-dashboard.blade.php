@extends('layouts.layout')
@section('content')
<div class="container-fluid">
 
	<div class="row">
	 
		<div class="col-md-12 navbar">
			<img src="https://i1.wp.com/vsee.com/wp-content/uploads/2015/05/VSee_Lab_Logo.png" alt="Logo" width="180" />			
				
			<div style="float: right;">
				<div class="user-info-block">
					<img alt="" src="/images/patient-image.png" width="18" align="left" class="user-info-image">
					<div class="dashboar-doctor-name">An Nguyen<span class="caret-down"><i class="fas fa-caret-down"></i></span></div>
				</div>
				<div class="computer-block">
					<img alt="" src="/images/computer.png" height="17">&nbsp;Test Computer
				</div>
			</div>	
													
			<hr class="w-100"/>							
		</div>		
	</div>

	<div class="row">
		
			<div class="col-lg-12 col-md-12 doctor-dashboard-margin-from-header">				
				<ul class="list-group dashboard" style="margin: 0 auto;">
				  	<li class="list-group-item list-group-item-success" style="background-color: #68a856"><img alt="" src="/images/hamburger-image.png" class="dashboard-hamburger-image"><span class="font-weight-bold text-white">&nbsp;Waiting Room</span>
				  		<span style="float: right;margin-right: 13px;" class="font-weight-bold text-white">Invite people</span>
				  		<img alt="" src="/images/invite-user.png" class="invite-user-image">			  		
				  	</li>
				  	<li class="list-group-item">
				  		<div id="listGroupItemDiv" class="dashboar-padding-top" data-bind="foreach: patients">
				  			<div class="dashboar-patient-block">
						  			<div class="dashboard-patient-image">
						  				<img alt="" src="/images/patient-image.png">
						  			</div>
						  			<div class="dashboard-patient-info">
						  				<div class="font-weight-bold" data-bind="text: patient_name">
						  				</div>
						  				<div class="dashboard-reason" data-bind="text: reason">
						  				</div>
						  				<div class="dashboard-email">
						  					patientone@vseelab.com
						  				</div>
						  			</div>
						  			<div class="dashboard-online-status">
						  				<div>
						  					<i class="fas fa-video video-icon"></i>
						  					Online
						  				</div>
						  				<div>
						  					<img alt="" src="/images/time-icon.png" class="dashboard-waiting-clock"> Waiting: <span data-bind="html: minWaitingText"></span> min
						  				</div>
						  			</div>
						  			<div class="dashboard-group-buttons">
						  				<button class="btn btn-warning dashboard-button"><img alt="" src="/images/chat-image.png"></button>
						  				<button class="btn btn-warning dashboard-button" data-bind="click: callButtonClick"><i class="fas fa-video video-icon text-white"></i></button>
						  				<button class="btn btn-warning dashboard-button"><img alt="" src="/images/dotdotdot.png"></button>
						  			</div>
						  		</div>
				  		</div>			  				
				  	</li>			  
				</ul>
				
			</div>
						
	</div>
</div>
<script type="text/javascript">
var ws;
var patients;
var viewModel = {
   patients: ko.observable(patients),    
};

if('WebSocket' in window)
{    
	ws = new WebSocket('ws://vseetest.com:8080');
	ws.onopen = function(){
	};

	// receive websocket event
	ws.onmessage = function(message){
		var responseData = JSON.parse(message.data);
		var html = '';
		switch(responseData.message)
		{
			case 'patientEnterConsultationRoom':
			    showPatientsList(false);
				break;
			case 'patientExitConsultationRoom':
			  	showPatientsList(false);
				break;
		}
	};	
}

function callButtonClick(patient)
{
   if(patient != undefined)
   {
     if(ws != null)
     {
        ws.send(JSON.stringify({ message: 'doctorCall',vseeId: patient.vseeId }));    
     }     
     location.href = 'vsee:demo@vsee.com';
     location.reload();
   }   
}

function ViewModel(patient)
{   
   this.id = patient.id;
   this.patient_name = patient.patient_name;
   this.reason = patient.reason;
   this.vseeId = patient.vseeId;
   this.created_date = patient.created_date;
   this.minuteWaiting = patient.minuteWaiting;
   this.minWaitingText = ko.observable(this.minuteWaiting);
   this.callButtonClick = callButtonClick;
   var self = this;
   setInterval(function(){     
     self.minuteWaiting++;
     self.minWaitingText(self.minuteWaiting);     
     $.post('/update-consultation-patient',self,function(response){
		
     });
   }, 60000);
}

function showPatientsList(applyBinding)
{
  $.getJSON("/patients-in-consultation-room",function(returnedData){
    patients = returnedData.patientsInConsultationRoom;
    for(var i=0;i<patients.length;i++)
    {
       patients[i] = new ViewModel(patients[i]);
    }

    viewModel.patients(patients);
    if(applyBinding)
    {
      ko.applyBindings(viewModel);
    }    
  });
}

showPatientsList(true);

</script>
@endsection
