@extends('layouts.layout')
@section('content')
<div class="container-fluid">
 
	<div class="row">
	 
		<div class="col-md-12 navbar">
			<img src="https://i1.wp.com/vsee.com/wp-content/uploads/2015/05/VSee_Lab_Logo.png" alt="Logo" width="180" />			
			
				
				<div style="float: right;display: block;width: 200px;">
					<img alt="" src="/images/computer.png" height="17">&nbsp;Test Computer
				</div>
						
			<hr class="w-100"/>							
		</div>		
	</div>

	<div class="row">
		<div class="col-md-3">
		</div>
		
		<div class="col-md-6 margin-from-header">
			<div class="text-center">
				<h3 class="waiting-room-header">Welcome to Code Challenge Waiting Room</h3>
				<h6>If this is an emergency, please call 911.</h6>
			</div>
			
			<ul class="list-group">
			  	<li class="list-group-item list-group-item-success" style="background-color: #68a856"><i class="fas fa-video text-white"></i><span class="font-weight-bold text-white">&nbsp;Talk to An Nguyen</span></li>
			  	<li class="list-group-item">
			  		<form data-bind="submit: enterWaitingRoomClick">
			  		  {{ csrf_field() }}	
					  <div class="form-group">
					    <label for="name" class="font-weight-bold">Please fill in you name to proceed <span class="require">*</span></label>
					    <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required="required">					    
					  </div>
					  <div class="form-group">
					    <label for="reason" class="font-weight-bold">Reason for visit <span class="text-secondary font-weight-normal">(optional)</span></label>
					    <textarea class="form-control" id="reason" name="reason" placeholder="Your reason for visit"></textarea>
					  </div>
					  <div class="form-group">
					    <label for="vseeId" class="font-weight-bold">VSee ID <span class="require">*</span></label>
					    <input type="text" class="form-control" id="vseeId" name="vseeId" placeholder="Your vsee id for doctor to call" required="required">
					  </div>
					  <button type="submit" class="btn btn-warning"><span class="font-weight-bold text-white">Enter Waiting Room</span></button>
					</form>		  		
			  	</li>			  
			</ul>
			
		</div>
		
		<div class="col-md-3">
		</div>
	</div>
</div>
<script type="text/javascript">
var viewModel = {
    enterWaitingRoomClick: function(formData){
      var data = $(formData).serialize();
      $.post('/enter-consultation-room', data, function(returnedData) {
          if(returnedData.responseCode == 1)
          {          
            if('WebSocket' in window)
            {
                //send websocket event
            	var ws = new WebSocket('ws://vseetest.com:8080');
            	ws.onopen = function(){
            	   ws.send(JSON.stringify({ 
            	      'message':'patientEnterConsultationRoom',
            		  'patient': returnedData.data
            	   }));		
            	   location.href = '/consultation-room';	   
            	};	
            }
            else
            {
              	location.href = '/consultation-room';
            }            
          }
      });
    },
};
ko.applyBindings(viewModel);
</script>
@endsection
