Instructions to deploy project in local:

1.Enable socket extentsion of PHP:

		_ Open php.ini file at your local and change this line to:
			extension=php_sockets.dll	
		
2.Download source code.

3.Run composer install command to download packages.

4.Setup a virtual host in apache:

		<VirtualHost *:80>
			ServerAdmin webmaster@host.example.com
			DocumentRoot "sourcecode-folder\public"
			ServerName vseetest.com
			ServerAlias vseetest.com

			<Directory "sourcecode-folder\public">
				AllowOverride All
				Order allow,deny
				allow from all
			</Directory>	
			ErrorLog sourcecode-folder\error.log
			CustomLog sourcecode-folder\access.log combined
		</VirtualHost>
	
5.Setup mySql database.

		_ create a database name vseetest

		_ run this script to create table :
			CREATE TABLE `consultation_room` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `patient_name` varchar(100) NOT NULL,
			  `reason` varchar(500) DEFAULT NULL,
			  `vseeId` varchar(45) NOT NULL,
			  `created_date` datetime NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
		
6.Setup config for project.

		_ Edit .env file in project , database section like this :
			DB_CONNECTION=mysql
			DB_HOST=127.0.0.1
			DB_PORT=3306
			DB_DATABASE=vseetest
			DB_USERNAME=<dbusername>
			DB_PASSWORD=<password>
		
7.Restart apache web server.

8.Run command to start PHP socket of project:

		php ConsultationRoomServer.php


After running 8 steps above , open a browser , type : vseetest.com then you will see waiting room screen.

## License

This project is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
